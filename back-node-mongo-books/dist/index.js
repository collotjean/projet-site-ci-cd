"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mongoose_1 = __importDefault(require("mongoose"));
const book_model_1 = __importDefault(require("./model/book.model"));
const body_parser_1 = __importDefault(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const app = express_1.default();
app.use(body_parser_1.default.json());
app.use(cors_1.default());
//const uri: string = "mongodb://localhost:27017/biblio";
const uri = "mongodb://mongo:27017/biblio";
mongoose_1.default.connect(uri, (err) => {
    if (err) {
        console.log(err);
    }
    else {
        console.log("Mongo db connection sucess ! ");
    }
});
app.get('/', (req, res) => {
    res.send("Hello World JB!");
});
app.get('/books', (req, res) => {
    book_model_1.default.find((err, books) => {
        if (err) {
            res.status(500).send(err);
        }
        else {
            res.send(books);
        }
    });
});
app.get('/books/:id', (req, res) => {
    book_model_1.default.findById(req.params.id, (err, book) => {
        if (err) {
            res.status(500).send(err);
        }
        else {
            res.send(book);
        }
    });
});
app.put('/books/:id', (req, res) => {
    book_model_1.default.findByIdAndUpdate(req.params.id, req.body, (err, book) => {
        if (err) {
            res.status(500).send(err);
        }
        else {
            res.send(book);
        }
    });
});
app.delete('/books/:id', (req, res) => {
    book_model_1.default.findByIdAndDelete(req.params.id);
});
app.get('/pbooks', (req, res) => {
    let p = parseInt(req.query.page) || 1;
    let size = parseInt(req.query.size) || 5;
    book_model_1.default.paginate({}, { page: p, limit: size }, (err, result) => {
        if (err) {
            res.status(500).send(err);
        }
        else {
            res.send(result);
        }
    });
});
app.get('/books-search', (req, res) => {
    let p = parseInt(req.query.page) || 1;
    let size = parseInt(req.query.size) || 5;
    let kw = req.query.kw || "";
    book_model_1.default.paginate({ title: { $regex: ".*(?i)" + kw + ".*" } }, { page: p, limit: size }, (err, result) => {
        if (err) {
            res.status(500).send(err);
        }
        else {
            res.send(result);
        }
    });
});
app.post('/books', (req, res) => {
    let book = new book_model_1.default(req.body);
    console.log("Book : " + book);
    book.save((err) => {
        if (err) {
            console.log("/books post fail!");
            res.status(500).send(err);
        }
        else {
            console.log("/books post sucess!");
            res.send(book);
        }
    });
});
app.listen(3000, () => {
    console.log("Server Started !");
});
