"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mongoose_1 = __importDefault(require("mongoose"));
const book_model_1 = __importDefault(require("../model/book.model"));
const app = express_1.default();
const uri = "mongodb://localhost:27017/biblio";
mongoose_1.default.connect(uri, (err) => {
    if (err) {
        console.log(err);
    }
    else {
        console.log("Mongo db connection sucess ! ");
    }
});
app.get('/', (req, res) => {
    res.send("Hello World !");
});
app.get('/books', (req, res) => {
    book_model_1.default.find((err, books) => {
        if (err) {
            res.status(500).send(err);
        }
        else {
            console.log("/books sucess!");
            res.send(books);
        }
    });
});
app.listen(3000, () => {
    console.log("Server Started !");
});
