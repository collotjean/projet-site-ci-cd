import express, {Request, Response} from 'express';
import mongoose from 'mongoose';
import Book from './model/book.model';
import bodyParser from 'body-parser';
import cors from 'cors';

const app = express();

app.use(bodyParser.json());
app.use(cors())

//const uri: string = "mongodb://localhost:27017/biblio";
const uri: string = "mongodb://mongo:27017/biblio";
mongoose.connect(uri, (err) => {
    if(err){
        console.log(err);
    } else {
        console.log("Mongo db connection sucess ! ");
    }
});

app.get('/', (req: Request, res: Response) => {
    res.send("Hello World JB!");
});

app.get('/books', (req: Request,res: Response) => {
    Book.find((err,books) => {
        if(err){
            res.status(500).send(err);
        } else {
            res.send(books);
        }
    });
});

app.get('/books/:id', (req: Request,res: Response) => {
    Book.findById(req.params.id, (err: any,book: any) => {
        if(err){
            res.status(500).send(err);
        } else {
            res.send(book);
        }
    });
});

app.put('/books/:id', (req,res) => {
    Book.findByIdAndUpdate(req.params.id, req.body, (err,book) => {
        if(err){
            res.status(500).send(err);
        } else {
            res.send(book);
        }
    });
});

app.delete('/books/:id', (req,res) => {
    Book.findByIdAndDelete(req.params.id);
});

app.get('/pbooks', (req: Request,res: Response) => {
    let p:number =  parseInt(req.query.page as string) || 1;
    let size:number = parseInt(req.query.size as string) || 5;
    Book.paginate({}, { page: p, limit: size }, (err, result) => {
        if(err){
            res.status(500).send(err);
        } else {
            res.send(result)
        }
    });
});

app.get('/books-search', (req: Request,res: Response) => {
    let p:number =  parseInt(req.query.page as string) || 1;
    let size:number = parseInt(req.query.size as string) || 5;
    let kw:string = req.query.kw as string || "";
    Book.paginate({title:{$regex:".*(?i)"+kw+".*"}}, { page: p, limit: size }, (err, result) => {
        if(err){
            res.status(500).send(err);
        } else {
            res.send(result);
        }
    });
});

app.post('/books', (req,res) => {
    let book = new Book(req.body);
    console.log("Book : " + book)
    book.save((err) => {
        if(err){
            console.log("/books post fail!");
            res.status(500).send(err);
        } else {
            console.log("/books post sucess!");
            res.send(book);
        }
    });
});

app.listen(3000,() => {
    console.log("Server Started !");
});
