import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

let bookSchema = new mongoose.Schema({
    title: {type: String, require: true},
    author: {type: String, require: true},
    price: {type: Number, require: true},
    publishingDate: {type: Date, require: true, default: new Date()},
    available: {type: Boolean, require: true, default: true},
    quantity: {type: Number, reqiure: true, default: 0}
});
bookSchema.plugin(mongoosePaginate);

const Book = mongoose.model("Book", bookSchema);
export default Book;
