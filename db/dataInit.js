var mi = require('mongoimport');

var json2mongo = require('json2mongo');

var data = [{
    "_id": {
      "$oid": "607ff1ad422583276cfba4f6"
    },
    "publishingDate": {
      "$date": "2021-04-21T09:34:33.419Z"
    },
    "available": true,
    "quantity": 0,
    "title": "",
    "price": 0,
    "__v": 0
  },{
    "_id": {
      "$oid": "607ff21e422583276cfba4f7"
    },
    "publishingDate": {
      "$date": "2021-04-08T00:00:00Z"
    },
    "available": true,
    "quantity": 515,
    "title": "sdvcds",
    "price": 515,
    "__v": 0
  },{
    "_id": {
      "$oid": "607ff667422583276cfba4f8"
    },
    "publishingDate": {
      "$date": "2021-04-14T00:00:00Z"
    },
    "available": true,
    "quantity": 5,
    "title": "fv",
    "price": 55,
    "__v": 0
  }]



var config = {
    fields: json2mongo(data),                     // {array} data to import
    db: 'biblio',                     // {string} name of db
    collection: 'books',        // {string|function} name of collection, or use a function to
    uri: "mongodb://mongo:27017",        // {string} [optional] by default is 27017
    callback: (err, db) => {
        if(err){
            console.log("Erreur initialisation des données : " + err);
        } else {
            console.log("Initialisation des données réussi !");
        }
    }       // {function} [optional]
    };

mi(config);